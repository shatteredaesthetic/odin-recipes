# odin-recipes

odin project: recipe page

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/shatteredaesthetic/odin-recipes.git
git branch -M main
git push -uf origin main
```
***

This is a recipe page for The Odin Project, meant to display competency in HTML. Later, there will be a chance to display CSS prowess.

It's not much, the the recipes are pretty good. You should try them.
